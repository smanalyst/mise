import 'dart:async';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:io' as io;
import 'package:device_info/device_info.dart';

class Mise {
  static const MethodChannel _channel = const MethodChannel('mise');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  String _applicationURL =
      'https://lfu8emwex0.execute-api.us-east-2.amazonaws.com/dev/application';
  String _errorURL =
      'https://lfu8emwex0.execute-api.us-east-2.amazonaws.com/dev/errors';
  String _saleURL =
      'https://lfu8emwex0.execute-api.us-east-2.amazonaws.com/dev/sales';
  String _userURL =
      'https://lfu8emwex0.execute-api.us-east-2.amazonaws.com/dev/users';

  String _apiKey = 'test';
  String get apiKey => _apiKey;

  /// API key from foodease
  set apiKey(String value) {
    if (value != null) {
      _apiKey = value;
    }
  }

  String _submerchantID = '';
  String get submerchantID => _submerchantID;

  /// UUID for your users at the account level
  set submerchantID(String value) {
    if (value != null) {
      _submerchantID = value;
    }
  }

  String _locationID = '';
  String get locationID => _locationID;

  /// UUID for your users at the restaurant level.
  /// if null, set to submerchantID
  set locationID(String value) {
    if (value != null) {
      _locationID = value;
    }
  }

  String _platform = '';
  String get platform => _platform;

  /// web, android, iOS, windows, linux, etc.
  set platform(String value) {
    if (value != null) {
      _platform = value;
    }
  }

  String _deviceID = '';
  String get deviceID => _deviceID;

  /// UUID for specific terminals
  set deviceID(String value) {
    if (value != null) {
      _deviceID = value;
    }
  }

  String _sessionID = '';
  String get sessionID => _sessionID;

  /// UUID for each login session
  set sessionID(String value) {
    if (value != null) {
      _sessionID = value;
    }
  }

  String _appVersion = '';
  String get appVersion => _appVersion;

  /// app version
  set appVersion(String value) {
    if (value != null) {
      _appVersion = value;
    }
  }

  var _customparam1 = '';
  get customparam1 => _customparam1;

  /// custom param, can be any type
  set customparam1(value) {
    if (value != null) {
      _customparam1 = value;
    }
  }

  var _customparam2 = '';
  get customparam2 => _customparam2;

  /// custom param, can be any type
  set customparam2(value) {
    if (value != null) {
      _customparam1 = value;
    }
  }

  var _customparam3 = '';
  get customparam3 => _customparam3;

  /// custom param, can be any type
  set customparam3(value) {
    if (value != null) {
      _customparam1 = value;
    }
  }

  var _customparam4 = '';
  get customparam4 => _customparam4;

  /// custom param, can be any type
  set customparam4(value) {
    if (value != null) {
      _customparam1 = value;
    }
  }

  var _customparam5 = '';
  get customparam5 => _customparam5;

  /// custom param, can be any type
  set customparam5(value) {
    if (value != null) {
      _customparam1 = value;
    }
  }

  /// map for pulling android device data
  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'name': build.brand,
      'systemName': build.product,
      'systemVersion': build.version.release,
      'model': build.model,
      'userID': build.androidId
    };
  }

  /// map for pulling iOS device data
  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.utsname.machine,
      'userID': data.identifierForVendor,
    };
  }

  // /// map for pulling browser device data
  // Map<String, dynamic> _readBrowserInfo(dynamic data) {
  //   return <String, dynamic>{
  //     'name': data.vendor,
  //     'systemName': data.product,
  //     'systemVersion': data.productSub,
  //     'model': data.platform,
  //     'userID': data.userAgent,
  //   };
  // }

  Map<String, dynamic> _deviceData;

  /// function to set _deviceData for the appropriate platform
  void setDeviceData() async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (io.Platform.isAndroid) {
      _deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
    } else if (io.Platform.isIOS) {
      _deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
    }
  }

  /// function to get values from _deviceData map
  dynamic _findKey(map, key) {
    return map.keys.firstWhere((k) => map[k] == key, orElse: () => '');
  }

  /// sends data to Mise's backend
  void _pushEvent(String url, Map<String, dynamic> json) {
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'x-api-key': apiKey,
    };
    var params = {"data": json, "partitionKey": apiKey};
    http.put(url, body: params, headers: headers);
  }

  void tagError(String errorName, String errorMessage) {
    var event = {
      "accountID": apiKey,
      "errorName": errorName,
      "errorMessage": errorMessage,
      "collectedTime": DateTime.now().millisecondsSinceEpoch,
      "platform": platform,
      "deviceID": deviceID,
      "appVersion": appVersion,
      "submerchantID": submerchantID,
      "locationID": locationID,
      "custom1": customparam1,
      "custom2": customparam2,
      "custom3": customparam3,
      "custom4": customparam4,
      "custom5": customparam5,
      "model": _findKey(_deviceData, "model"),
      "name": _findKey(_deviceData, "name"),
      "systemName": _findKey(_deviceData, "systemName"),
      "systemVersion": _findKey(_deviceData, "systemVersion"),
      "userID": _findKey(_deviceData, "userID"),
    };

    _pushEvent(_errorURL, event);
  }

  void tagUser() {
    var event = {
      "accountID": apiKey,
      "collectedTime": DateTime.now().millisecondsSinceEpoch,
      "deviceID": deviceID,
      "appVersion": appVersion,
      "submerchantID": submerchantID,
      "locationID": locationID,
      "userlang": io.Platform.localeName,
      "custom1": customparam1,
      "custom2": customparam2,
      "custom3": customparam3,
      "custom4": customparam4,
      "custom5": customparam5,
    };

    _pushEvent(_userURL, event);
  }

  void tagPerformance(Function callback, List args) {
    Stopwatch stopwatch = new Stopwatch()..start();
    Function.apply(callback, args);
    var _executedTime = stopwatch.elapsedMilliseconds;
    var event = {
      "accountID": apiKey,
      "collectedTime": DateTime.now().millisecondsSinceEpoch,
      "deviceID": deviceID,
      "appVersion": appVersion,
      "submerchantID": submerchantID,
      "locationID": locationID,
      "execTime": _executedTime,
      "functionID": callback.toString(),
      "custom1": customparam1,
      "custom2": customparam2,
      "custom3": customparam3,
      "custom4": customparam4,
      "custom5": customparam5,
      "model": _findKey(_deviceData, "model"),
      "name": _findKey(_deviceData, "name"),
      "systemName": _findKey(_deviceData, "systemName"),
      "systemVersion": _findKey(_deviceData, "systemVersion"),
      "userID": _findKey(_deviceData, "userID"),
    };

    _pushEvent(_applicationURL, event);
  }

  void tagSale(double amount, String transactionType, String status,
      int saleTime, List order) {
    var event = {
      "accountID": apiKey,
      "collectedTime": DateTime.now().millisecondsSinceEpoch,
      "deviceID": deviceID,
      "appVersion": appVersion,
      "submerchantID": submerchantID,
      "locationID": locationID,
      "amount": amount,
      "transactiontype": transactionType,
      "status": status,
      "saleTime": saleTime,
      "order": order,
      "custom1": customparam1,
      "custom2": customparam2,
      "custom3": customparam3,
      "custom4": customparam4,
      "custom5": customparam5,
    };

    _pushEvent(_saleURL, event);
  }
}

/// end of mise
