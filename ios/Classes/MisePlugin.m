#import "MisePlugin.h"
#if __has_include(<mise/mise-Swift.h>)
#import <mise/mise-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "mise-Swift.h"
#endif

@implementation MisePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMisePlugin registerWithRegistrar:registrar];
}
@end
